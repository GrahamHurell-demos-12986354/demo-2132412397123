# Movie Night Trivia Quiz


Demo video : `https://streamable.com/gagkmt`

Comments: 

1 ) Quiz questions could be randomized more, too much similarity across quizzes.
2 ) Mobile device testing is not comprehensive. I do not have a locally accessible server, or remote vps for doing live mobile testing. Only browser simulators have been used.


## Stack

### NodeJs Web Scraper

Directory : `/web-scraper`

Src : `https://badge.fury.io/js/imdb-top250-node`



#### Nodejs - API & DB

Directory : `/api`

Created using `FeathersJs`
`https://github.com/feathersjs/feathers`



#### Socket.io Realtime Messaging Relay

Directory : `/socket-io-relay`

Created using `Socket.io` & `Express`


#### Reactjs - frontend

Libraries : `Tachyons.css`, `Blueprint` ( Icons )

To run : 

* Edit ``./src/config.json`` with your API endpoint URL


```
{
    "api": "http://localhost:3030",
    "socket_url": "http://localhost:8001"
}
```


To find this url, first deploy the FeathersJS API server to your local or remote VPS.

* Run `npm run build` to build the app or start the local react development server using `npm start`

* Deploy using the tool of your choice, recommended options are Netlify or Surge.sh

* Navigate to `./src/build`
* Install `surge.sh` using `npm install --global surge`, then Deploy using `surge` * Install `netlify-cli` using `npm install netlify-cli -g` then deploy using `netlify deploy`
* You will now have a URL for the deployed site


# Instructions 

* Clone the repo into a local or remote server.

* Start the FeathersJs app ( DB & API )

* Navigate to `./main/server/api`, run `npm install`, then `npm start`
* Navigate to `./main/server/socket-io-relay`, run `npm install`, then `npm start`
* Now your Database & API, and Realtime Socket IO server are running. 



* Run the web scraper app to scrape data from IMBD, convert to JSON and insert into DB. After complete, exit as this is a once off process.

* Navigate to `./web-scraper`
* run `npm install` to install node_modules
* Edit the `config.json` file with the correct API endpoint URL ( or use default config ).

```
{
    "api": "http://localhost:3030"
}
```

* Run the app with `npm start`

* Get data :
 
* If running locally, navigate to `http://localhost:3001` in your browser.
* If running on remote server, navigate to `http://<YOUR_IP>:3001` in your browser.

This triggers the web scraping function and inserts the data into the running `FeathersJS NEDB Database` via the `FeathersJS API`.

Load the client app, see `ReactJs - Frontend `


