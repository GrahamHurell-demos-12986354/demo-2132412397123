import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// import imdb_data from './json/imdb-top250.json'

import { AppContext, config } from 'utils'

import { fetch_movies } from './Methods'

// const SOCKET_URI = config.socket_url

class Data extends PureComponent {


  constructor(props) {

    super(props)

    this.state = {

      hasError: false,

      movies: [],

      points_a: 0,
      points_b: 0,

      responses_a: [],
      responses_b: [],

    }

  }

  componentWillMount = async() => {

    await fetch_movies({ self: this })
  
  }

  componentDidMount = () => {
  
  }


  render() {

    const { movies, points_a, points_b } = this.state

    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }

    return (
      <AppContext.Provider

        value={{

          movies: movies,

          points_a, points_b,

          Data: this,

          ...this.context

        }}

      >

        {this.props.children}

      </AppContext.Provider>
    );
  }
}

Data.propTypes = {
  // bla: PropTypes.string,
};

Data.defaultProps = {
  // bla: 'test',
};

export default Data;
Data.contextType = AppContext
