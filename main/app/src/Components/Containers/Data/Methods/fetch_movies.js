import { config } from 'utils'

const fetch_movies = async(op) => {

    let { self } = op 

    let api_url = config.api + "/movies/"

    return new Promise( async resolve => {

        // http://localhost:3030/movies?$limit=250


        await fetch( api_url )

        .then( res => res.json() )

        .then( async res => {

              self.setState({

                  movies: res.data
              
                })  

        })


    })
}

export default fetch_movies