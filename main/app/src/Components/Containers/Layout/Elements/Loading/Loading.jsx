import React from 'react';

import PropTypes from 'prop-types';

import { Spinner } from '@blueprintjs/core'
//import { Test } from './Loading.styles';

const Loading = (props) => (
  <div 
  
  style={{

    height: '100vh',
    
    width: '100vw'

  }}

  className=" absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center "

  >
  
    <Spinner style={{ top: '6.5vh'}} className=" relative " size={40} />
  
  </div>
);

Loading.propTypes = {
  // bla: PropTypes.string,
};

Loading.defaultProps = {
  // bla: 'test',
};

export default Loading;
