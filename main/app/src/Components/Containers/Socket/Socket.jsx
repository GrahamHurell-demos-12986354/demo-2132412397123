import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { AppContext } from 'utils'
import io from "socket.io-client";

var socket

const SOCKET_URI = "http://localhost:8002"


class Socket extends PureComponent {

  socket = null

  constructor(props) {

    super(props)

    this.state = {

      hasError: false,

    }

  }

  onMessageReceived = async (message) => {

    console.log(' ')
    console.log('Data : Message Received ===> ', message)
    console.log(' ')

    let data = JSON.parse(message.e)

    let Auth = this.context.Auth

    let Data = this.context.Data

    if (Auth.state.player_name === data.player) {

      let answers = Data.state.responses_a

      answers.push(data)

      Data.setState({

        points_a: data.points,
        responses_a: answers

      })

    }

    if (Auth.state.opponent_name === data.player) {

      let answers = Data.state.responses_b

      answers.push(data)

      Data.setState({

        points_b: data.points,
        responses_b: answers,

      })

    }


  }

  onQuizAnswer = (data) => {

    this.socket.emit("message", JSON.stringify(data))

  }

  submitLogin = (name) => {

    let { context } = this
    let { Auth } = context
  
    // Auth.setState({ user: name, player_name: name.name, player_id: name.id })

    console.log('Auth : state ===> ', Auth.state )

    this.socket.emit( "login", JSON.stringify(name) )

  }

  onLogin = async (message) => {

    let { Auth } = this.context

    console.log('onLogin', message )

    Auth.on_login({ self: this , message } )

  }

  initSocketConnection = () => {

    this.socket = io.connect(SOCKET_URI)

    window.socket = this.socket

  }

  setupSocketListeners = () => {

    this.socket.on("disconnect", this.onClientDisconnect);
    this.socket.on("message", this.onMessageReceived)
    this.socket.on("login", this.onLogin)

  }

  onClientDisconnected = () => {


    console.log("[[ Socket.io server disconnected. ]]");

  }

  componentDidMount = () => {

    this.initSocketConnection()

    this.setupSocketListeners()

  }

  onQuizAnswer = (data) => {

    this.socket.emit("message", JSON.stringify(data))

  }

  onReconnection = () => {

    console.log("[[ Socket.io server reconnected. ]]");

  }

  onConnect = () => {

    console.log("[[ Socket.io server connected. ]]")

  }


  render() {

    if (this.state.hasError) {

      return <h1>Something went wrong.</h1>;

    }

    return (

      <AppContext.Provider

        value={{

          Socket: this,

          ...this.context

        }}

      >
        {this.props.children}
      </AppContext.Provider>

    );
  }
}

Socket.propTypes = {
  // bla: PropTypes.string,
};

Socket.defaultProps = {
  // bla: 'test',
};

export default Socket;

Socket.contextType = AppContext
