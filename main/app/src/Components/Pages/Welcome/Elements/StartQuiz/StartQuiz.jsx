import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from '@blueprintjs/core'
import history from '../../../../../Utils/history/history.js'
//import { Test } from './StartQuiz.styles';

const StartQuiz = ({ self, active }) => (
  <div className="StartQuizWrapper flex flex-row">
      <button 
      
      onClick={ () => {

          active && history.push('/')

      }}
      
      className={ ( active ? " bg-green-highlight " : " bg-white-20 " ) + ( " pointer ph4 pv4 f5 fw5 white bn br1 bs-b items-center justify-center flex flex-row w-100 " ) }>
        <span className="mr3">{ active ? "Start the game" : "Waiting for other player"}</span>{ !active &&  <Spinner style={{ top: '6.5vh'}} className=" relative " size={25} /> }
      </button>
  </div>
);

StartQuiz.propTypes = {
  // bla: PropTypes.string,
};

StartQuiz.defaultProps = {
  // bla: 'test',
};

export default StartQuiz;
