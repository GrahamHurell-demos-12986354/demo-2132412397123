import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { WelcomeMessage, WaitingRoom, StartQuiz } from './Elements'
import { Wrapper } from '../Elements'
import { AppContext } from 'utils'

class Welcome extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
 
      <Wrapper >

        <AppContext.Consumer>

          { props => (

            <div className=" bg-black-50 flex flex-column h-100 pa5">
            
              <WelcomeMessage context={props} />
              <WaitingRoom context={props} active={props.Auth.state.opponent_name}  />
              <StartQuiz context={props} active={props.Auth.state.opponent_name} />
            
            </div>

          )}

        </AppContext.Consumer>


      </Wrapper>

    );
  }
}

Welcome.propTypes = {
  // bla: PropTypes.string,
};

Welcome.defaultProps = {
  // bla: 'test',
};

export default Welcome;
