import React from 'react'

import { Welcome } from '../../Home/Elements/Main/Elements/index.js'
import { submit } from '../Actions'

const Form = ({ self, context }) => (

    <div

        style={{
            backgroundImage: 'url(/welcome.jpg)'
        }}

        className=" relative flex flex-column w-100 h-100 items-center justify-end bg-white bg-cover bg-center ">

        <div 
        
        style={{
            zIndex: 1
        }}

        className="h-100 w-100 absolute bg-black-50 top-0 left-0 right-0 bottom-0 mw6 center"></div>


        {/* <Welcome context={ context } /> */}

        <span className=' flex pt4 f5 fw5 white tc pv4 '>{"Enter your name to begin"}</span>

        <div

            id="Login_Form"

            className=" flex flex-column items-center justify-end w-100 mw6 center ph5 pb5 bottom-0 relative  "

            style={{
                zIndex: 5
            }}

        >

            <div className=" form-row flex flex-column w-100 ">

                <input
                    onKeyPress={

                        (e) => {

                            console.log(e.key)

                            if (e.key === "Enter") {

                                // e.preventDefault()

                                submit({ self, context, name: e.target.value })

                            }

                        }

                    }
                    type={'text'}
                    placeholder={"Player name"}
                    className=" flex flex-column ph4 pv4 br1 -round f5 fw5 black bn w-100 bs-c "

                />

            </div>

        </div>

    </div>
)

export default Form 