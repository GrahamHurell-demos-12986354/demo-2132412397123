// import {Auth} from '../../../../../../../../../../../../../../CREW/src/components/Containers/index'
const select_option = async (op) => {

    const { self, option, slide, index, year } = op

    self.setState({ ready: false })

    let { state } = self
    let { options } = state

    let options_ = options

    let { context } = self.props.self
    let { Auth, Data, Layout, Socket } = context
    let points = Data.state.points_a
    let answers = Data.state.responses_a

    let answer

    if (option.year === year) {

        option.correct = true

        option.incorrect = false

        points = points + 5

        // answer = { slide: slide, option: option, correct: true, points: points }

        Layout.setState({

            quiz_state: 'correct'

        })

    }

    if (option.year !== year) {

        option.correct = false

        option.incorrect = true

        points = points - 3

        // answer = { slide: slide, option: option, correct: false, points: points }

        Layout.setState({

            quiz_state: 'incorrect'

        })

    }

    options_[index] = option

    Layout.setState({

        quiz_response: true,
        quiz_correct: option.year === year ? true : false

    })

    let player = Auth.state.player_name

    console.log('Data ===> ', Data )

    Socket.onQuizAnswer({

        slide, option, player, points, answer 
    
    })

    // answers.push(answer)

    // Data.setState({

    //     points_a: points,
    //     // responses_a: answers
                    
    // })

    self.setState({

        options: options_

    })

    setTimeout(() => {

        self.setState({

            ready: true

        })

    }, 10)


    return (options_)
}

export default select_option