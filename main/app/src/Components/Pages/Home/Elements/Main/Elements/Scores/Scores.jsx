import React from 'react'
import PropTypes from 'prop-types'
// import { Test } from './Score.styles'
import { Icon } from '@blueprintjs/core'

const isMobile = true

const Scores = ({ context, active }) => (
  <div id='Scores' style={isMobile

    ?

    {
      height: '80%',
      top: '10vh',
      transform: active ? 'translate3d(0,0vh,0)' : 'translate3d(0,90vh,0)'
    }

    :

    {
      height: '80%',
      top: '10vh',
      transform: active ? 'translate3d(0,0vh,0)' : 'translate3d(0,90vh,0)'
    }} className={ (active ? ' o-1 z-9 ' : ' o-0 z-01 ')

      + (' absolute flex w-100 h-100 black items-center justify-center h-100 mw6 center overflow-auto')}>
        <div className='flex flex-row  justify-between h-100 w-100'>
          <div className='w-50 flex flex-column br b--black-10 bw1 '>
            <span className=' w-100 tc black-50 pv3 bb b--black-05'>{"Answers"}</span>
            <div className='flex flex-column  '>
              {context.Data.state.responses_a.map((response, index) => (
                <ScoreItem response={response} />
              ))}
            </div>
          </div>
          <div className='w-50 flex flex-column'>
            <span className=' w-100 tc black-50 pv3 bb b--black-05'>{"Answers"}</span>
            <div className='flex flex-column '>
              {context.Data.state.responses_b.map((response, index) => (
                <ScoreItem response={response} />
              ))}
            </div>
          </div>
        </div>
  </div >
)

Scores.propTypes = {
  // bla: PropTypes.string,
}

Scores.defaultProps = {
  // bla: 'test',
}

export default Scores

const ScoreItem = ({ response }) => (
  response && <div className='  flex flex-column f5 fw5 black pv3 ph4 bb b--black-05'>
    <div className=' flex flex-row f5 fw5 black '>
      {response && response.option && response.option.correct
        ? <Icon icon={'small-tick'} iconSize={20} className=' black-50 mr3 ' />
        : <Icon icon={'small-cross'} iconSize={20} className=' black-50 mr3 ' />}
      {response && response.slide.name}
    </div>
    {response && response.option && parseInt(response.slide.year) !== response.option.year &&
      <div className=' flex flex-column f5 fw5 black-50 pt3 '>
        <div className='flex flex-row'>
          {"Correct Answer"}<span className='fw6 ml3'>{response && response.slide.year}</span>
        </div>
      </div>}
  </div> || <></>
)
