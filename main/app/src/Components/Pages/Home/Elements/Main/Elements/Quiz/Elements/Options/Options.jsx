import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { initialize } from './Methods'
import { select_option } from './Actions'

class Options extends PureComponent {

  constructor(props) {

    super(props)

    this.state = {

      hasError: false,

      options: [],

      result: null,

      ready: false

    }

  }


  componentDidMount = async () => {

    let { year } = this.props

    let self = this

    await initialize({ self, year })

  }


  checkPriorResponse = (option) => {

    let { year, slide, self, active } = this.props

    let context = self.context

    let { Data } = context

    let classname


    if (active && !Data.state.quiz_response ) {

      if (Data.state.responses_a.length > 0) {

        if (Data.state.responses_a.filter(a => a.slide._id === slide._id).length > 0) {

          if (Data.state.responses_a.filter(a => a.slide._id === slide._id)[0]) {

            let response = Data.state.responses_a.filter(a => a.slide._id === slide._id)[0]


            if (parseInt(option.year) === parseInt(response.option.year)) {

              if (response.correct) {

                classname = (classname + " fw6 bg-green-highlight white ")

                this.setState({
                  result: true
                })

              }

              else {

                classname = (classname + " bg-red white ")

                this.setState({
                  result: false
                })

              }

            }

            return classname
          }

        }

      }
    }

  }

  render() {

    let { state, props } = this
    let { options, ready } = state
    let { year, self, slide } = props
    let { context } = self
    let { Data, Layout } = context

    let ready_ = ready && !Layout.state.quiz_response && options

    if (this.state.hasError) {

      return <h1>Something went wrong.</h1>;

    }

    return (

      ready_ &&

      <div
        style={{
          bottom: '18vh'
        }}
        className="flex flex-column  left-0 right-0 ">

        <div className="flex flex-column pt3 pb2 f5 fw5 black-50 tc">

          {this.state.result === null ? "Guess the year" : this.state.result === true ? "Correct" : "Incorrect"}

        </div>

        {
          this.state.result !== null &&
          <div className="flex flex-column pb3 pt2 f4 fw5 tc">

            {this.state.result === true ? <span className=" green ">{"5 points"}</span> : <span className=" red ">{"-3 points"}</span> || null}

          </div>
        }

        <div

          id="Options"

          className="ph0-ns ph0  flex flex-row flex-wrap items-center justify-center mt3-ns mt2  "

        >


          {options.map((option, index) => (


            <div

              onClick={() => this.state.result === null && select_option({ self: this, option, slide, index, year })}

              className={
                (this.checkPriorResponse(option))
                + ( this.state.result === null && " grow hover-black " )
                + (option.incorrect && " bg-red white ")
                + (option.correct && " bg-white green ")
                + (" pointer trans-b flex flex-column flex-wrap ph4 pv2 br2 f4 fw6  black-60 ba-b--black-05 mr2 mb2 hover-bg-white-30 hover-bn ")}>

              {option.year}
              {option.correct}

            </div>

          ))}

        </div>

      </div>

      || null
    );
  }
}

Options.propTypes = {
  // bla: PropTypes.string,
};

Options.defaultProps = {
  // bla: 'test',
};


export default Options