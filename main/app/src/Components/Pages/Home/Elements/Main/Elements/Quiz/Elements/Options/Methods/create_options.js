const create_options = (year) => {

  return new Promise(async resolve => {

    let mods = [
      -5, 10, 0, -3, 5, 1
    ]

    let options = []

    for (let mod of mods) {

      let y = (parseInt(year) + mod)

      let option = {
        year: y,
        correct: false,
        incorrect: false
      }

      options.push(option)

    }

    resolve(options)

  })
}

export default create_options