import React from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Welcome.styles';

const Welcome = (props) => (
 <div 
 style={{
   zIndex: 5
 }}
 className=" flex flex-column items-center justify-end w-100 h-100 mw6 center ph5 pb3 ">

      <span className=" flex pt3 pb2 f5 fw4 white tc ">{"Welcome to"}</span>
      <span className=" flex pb3 pt2 f2 fw6 white tc ">{"Movie Night Trivia"}</span>
     
      <span className="  flex pv2 f4 fw4 white-80 tc mw6 ph3 ">{"A game consists of 8 rounds with 5 points allocated for a correct answer and -3 for an incorrect answer. The player with the highest score at the end of the game wins. "}</span>
      <span className=" flex pt4 f5 fw6 white tc ">{"Enter your name to begin"}</span>
 
 </div>
);

Welcome.propTypes = {
  // bla: PropTypes.string,
};

Welcome.defaultProps = {
  // bla: 'test',
};

export default Welcome;
