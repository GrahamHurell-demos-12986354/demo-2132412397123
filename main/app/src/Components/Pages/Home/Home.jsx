import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Header, Footer, Scores, Main } from './Elements'
import { Wrapper } from '../Elements' 
import { AppContext } from 'utils'
import { Controls } from './Elements/Main/Elements/index.js'


class Home extends PureComponent {

  constructor(props) {
    
    super(props)

    this.state = {
      
      hasError: false,

    }

  }

  render() {

    const { context } = this

    const { theme, player_scores } = context

    if (this.state.hasError) {

      return <h1>Something went wrong.</h1>

    }

    return (

      <AppContext.Consumer>

        {(props) => (

          <Wrapper ctx={ props } thus={ this } quiz_state={props.Layout.state.quiz_state} theme={props.theme} >

            
            <Header ctx={ props } thus={ this } scores_state={props.Data.state} context={props} />

            <Main ctx={ props } thus={ this } context={props} self={this} />

            <Scores ctx={ props } thus={ this } context={props} active={props.player_scores} />

            <Controls self={this} context={props} />

            <Footer ctx={ props } thus={ this } />


          </Wrapper>

        )}

      </AppContext.Consumer>

    )
  }
}

Home.propTypes = {
  // bla: PropTypes.string,
};

Home.defaultProps = {
  // bla: 'test',
};

export default Home

Home.contextType = AppContext

