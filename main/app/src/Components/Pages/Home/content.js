const config = {
    
    site_url: "",
    socket: "",
    api: "",

    keys: {


        mapbox: "",
        here: "",
        jsonbin: "",
        

        google_map: {
            id: "",
            key: ""
        },
        google: {
            id: "",
            key: ""
        },
        instagram: {
            id: "",
            key: ""
        },
        facebook: {
            id: "",
            key: ""
        },
        twitter: {
            id: "",
            key: ""
        },
        tiktok: {
            id: "",
            key: ""
        },
        contentful: {
            id: "",
            key: ""    
        },


    }
    

}

const content = {

    logo_url: "",
    header_text: "Movie Trivia",
    footer_text: "Copyright 2020",
    footer_icon: "info",
    welcome_text:  {
        app_name: 'Movie Night Trivia',
        introduction: "A game consists of ...",
        login_label: "Enter your name to begin"
    },

}