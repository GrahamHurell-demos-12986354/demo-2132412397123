import React from 'react'

const Wrapper = ({ children, quiz_state }) => (
    <section

        id="Wrapper"

        style={{
            maxWidth: '27rem',
            minHeight: '780px'
        }}

        className={
            (
                quiz_state && (quiz_state === "normal" ? " bg-white "
                    : quiz_state === "correct" ? " bg-green-highlight "
                        : quiz_state === "incorrect" ? " bg-red " : " bg-light-green ") || " bg-white "
            )
            + (" relative trans-d flex flex-column items-center justify-center mv5-ns center br3-ns overflow-hidden bs-b h-100 bg-cover bg-center ")} >

        {children}

    </section>
)

export default Wrapper