import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AppContext } from 'utils'

function PrivateRoute({ component: Component, ...rest }) {

    return (
        
        <AppContext.Consumer>

            {
                context_props => (

                    <Route

                        {...rest}

                        render={props =>

                            context_props.Auth.state.isAuthenticated ? (

                                <Component {...props} />

                            ) : (

                                    <Redirect to="/login" />

                                )

                        }

                    />

                )
            }

        </AppContext.Consumer>
    )

}

export default PrivateRoute

// PrivateRoute.contextType = AppContext