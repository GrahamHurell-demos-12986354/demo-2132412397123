import React from 'react';
import { Route, Router, Switch } from "react-router-dom";
import PrivateRoute from './Utils/private.js'
import history from "./Utils/history/history.js";

import { Auth, Data, Layout, Control, Socket } from './Components/Containers'
import { Home, Welcome, Login } from './Components/Pages'

import './Assets/css/index.css';



function App() {

  return (


    <Router history={history}>

      <Switch>

        <Auth>

          <Data>

            <Control>

              <Layout>

                <Socket>

                  <PrivateRoute exact path={"/"} component={Home} />

                  <PrivateRoute exact path={"/welcome"} component={Welcome} />

                  <Route exact path={"/login"} component={Login} />

                </Socket>

              </Layout>

            </Control>

          </Data>

        </Auth>

      </Switch>

    </Router>

  );
}

export default App;
