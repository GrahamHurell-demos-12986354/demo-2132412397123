const db = require('./db/db.service.js');
const players = require('./players/players.service.js');
const movies = require('./movies/movies.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(db);
  app.configure(players);
  app.configure(movies);
};
