const express = require("express");
const app = express();
const port = 8002;
var server = require("http").Server(app);
const io = require("socket.io")(server);
const cors = require("cors");

app.use(cors());

var clients = []
var players = []

io.on("connection", function (client) {

  console.log("Client connected", client.id)

  console.log('Clients', clients)

  client.on("login", e => {

    players.push(e)

    clients.push(client.id)

    if (players.length > 10) {
    
      players.shift()
    
    }

    io.emit('login', { e, clients, players })

    client.emit('login', { e, clients, players })
    
    console.log('Login', e )
    
    console.log('Players', players )

    console.log('Clients', clients )

  })

  client.on("message", e => {

    console.log("message", e)

    io.emit('message', { e, clients })

  })

  io.on("message", e => {

    console.log("message", e )

    io.emit('message', { e, clients })

  })

  io.on("system", e => {

    console.log("system", e)

    io.emit('system', { e, clients })

  })

  client.on("disconnect", function () {

    client.emit('disconnect', client.id)
    
    console.log('client disconnected', client.id)

  })

  io.on("disconnect", function () {

    io.emit('disconnect', client.id)
    
    let i = clients.findIndex(a => a.id === client.id)
    
    clients.splice(i, 1)

    console.log('client disconnected', client.id)

    console.log('clients', clients)

  })

})

app.get("/clients", (req, res) => {

  res.send({ data: clients })

})

app.get("/players", (req, res) => {

  res.send({ data: players })

})

server.listen(port, () =>

  console.log(`Example app listening on port ${port}!`)

)
